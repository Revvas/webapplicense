
```shell script
cp ./config.example.py ./config.py
```

```shell script
wget -O LicenseDATA.zip http://isga.obrnadzor.gov.ru/rlic/opendata/
unzip LicenseDATA.zip
ln -s ./data-????????-structure-20150421.xml LicenseDATA.xml

wget -O CertificateDATA.zip http://isga.obrnadzor.gov.ru/accredreestr/opendata/
unzip CertificateDATA.zip
ln -s ./data-????????-structure-20160713.xml CertificateDATA.xml
```

```shell script
./run.py parse license ./data/LicenseDATA.xml
./run.py parse certificate ./data/CertificateDATA.xml
```
