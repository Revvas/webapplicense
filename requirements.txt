Flask>=1.1.1
Flask-SQLAlchemy>=2.4.1
Flask-marshmallow>=0.10.1
marshmallow-sqlalchemy>=0.21.0
python-dotenv>=0.10.3
