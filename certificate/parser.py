
from lxml import etree

from app import db
from .models import Certificate, ActualEducationOrganization, Supplement, EducationalProgram, Decision


session = db.session


def fast_iter( context, func ):
	for event, elem in context:
		func( elem )
		session.commit()
		elem.clear()
		print( "session complete" )
		while elem.getprevious() is not None:
			del elem.getparent()[0]
	del context


class Parser( object ):
	def __init__( self ):
		pass

	def run( self, infile ):
		context = etree.iterparse( infile.absolute().as_posix(), events=('end',), tag='Certificate' )
		fast_iter( context, self.certificate )

	def certificate( self, elem ):
		certificate = Certificate()
		session.add( certificate )

		######## Certificate ; Parent ########
		certificate.isfederal= elem.xpath( './IsFederal' ).pop().text
		certificate.id_from_xml = elem.xpath( './Id' ).pop().text
		certificate.statusname= elem.xpath( './StatusName' ).pop().text
		certificate.typename= elem.xpath( './TypeName' ).pop().text
		certificate.regionname= elem.xpath( './RegionName' ).pop().text
		certificate.regioncode= elem.xpath( './RegionCode' ).pop().text
		certificate.federaldistrictname= elem.xpath( './FederalDistrictName' ).pop().text
		certificate.federaldistrictshortname= elem.xpath( './FederalDistrictShortName' ).pop().text
		certificate.regnumber= elem.xpath( './RegNumber' ).pop().text
		certificate.serialnumber= elem.xpath( './SerialNumber' ).pop().text
		certificate.formnumber= elem.xpath( './FormNumber' ).pop().text
		certificate.issuedate= elem.xpath( './IssueDate' ).pop().text
		certificate.enddate= elem.xpath( './EndDate' ).pop().text
		certificate.controlorgan= elem.xpath( './ControlOrgan' ).pop().text
		certificate.postaddress= elem.xpath( './PostAddress' ).pop().text
		certificate.eduorgfullname= elem.xpath( './EduOrgFullName' ).pop().text
		certificate.eduorgshortname= elem.xpath( './EduOrgShortName' ).pop().text
		certificate.eduorginn= elem.xpath( './EduOrgINN' ).pop().text
		certificate.eduorgkpp= elem.xpath( './EduOrgKPP' ).pop().text
		certificate.eduorgogrn= elem.xpath( './EduOrgOGRN' ).pop().text
		certificate.individualentrepreneurlastname= elem.xpath( './IndividualEntrepreneurLastName' ).pop().text
		certificate.individualentrepreneurfirstname= elem.xpath( './IndividualEntrepreneurFirstName' ).pop().text
		certificate.individualentrepreneurmiddlename= elem.xpath( './IndividualEntrepreneurMiddleName' ).pop().text
		certificate.individualentrepreneuraddress= elem.xpath( './IndividualEntrepreneurAddress' ).pop().text
		certificate.individualentrepreneuregrip= elem.xpath( './IndividualEntrepreneurEGRIP' ).pop().text
		certificate.individualentrepreneurinn= elem.xpath( './IndividualEntrepreneurINN' ).pop().text

		# ActualEducationOrganization
		for el_actualeducationorganization in elem.xpath( './ActualEducationOrganization' ):
			self.actualeducationorganization( el_actualeducationorganization, certificate )

		# Supplements
		el_supplements = elem.xpath( './Supplements' ).pop()
		for el_supplement in el_supplements.iterchildren( tag='Supplement' ):
			self.supplement( el_supplement, certificate )

		# Decisions
		el_decisions = elem.xpath( './Decisions' ).pop()
		for el_decision in el_decisions.iterchildren( tag='Decision' ):
			self.decision( el_decision, certificate )

	def actualeducationorganization( self, el_actualeducationorganization, certificate_or_supplement ):
		if len( el_actualeducationorganization ) == 0:
			return

		acteducorg = (
			session.query( ActualEducationOrganization )
				.filter( ActualEducationOrganization.id_from_xml == el_actualeducationorganization.xpath( './Id' ).pop().text )
			).one_or_none()

		if acteducorg is None:
			acteducorg = ActualEducationOrganization()

		certificate_or_supplement.actual_education_organization = acteducorg

		acteducorg.id_from_xml = el_actualeducationorganization.xpath( './Id' ).pop().text
		acteducorg.fullname= el_actualeducationorganization.xpath( './FullName' ).pop().text
		acteducorg.shortname= el_actualeducationorganization.xpath( './ShortName' ).pop().text
		acteducorg.headeduorgid= el_actualeducationorganization.xpath( './HeadEduOrgId' ).pop().text
		acteducorg.isbranch= el_actualeducationorganization.xpath( './IsBranch' ).pop().text
		acteducorg.postaddress= el_actualeducationorganization.xpath( './PostAddress' ).pop().text
		acteducorg.phone= el_actualeducationorganization.xpath( './Phone' ).pop().text
		acteducorg.fax= el_actualeducationorganization.xpath( './Fax' ).pop().text
		acteducorg.email= el_actualeducationorganization.xpath( './Email' ).pop().text
		acteducorg.website= el_actualeducationorganization.xpath( './WebSite' ).pop().text
		acteducorg.ogrn= el_actualeducationorganization.xpath( './OGRN' ).pop().text
		acteducorg.inn= el_actualeducationorganization.xpath( './INN' ).pop().text
		acteducorg.kpp= el_actualeducationorganization.xpath( './KPP' ).pop().text
		acteducorg.headpost= el_actualeducationorganization.xpath( './HeadPost' ).pop().text
		acteducorg.headname= el_actualeducationorganization.xpath( './HeadName' ).pop().text
		acteducorg.formname= el_actualeducationorganization.xpath( './FormName' ).pop().text
		acteducorg.kindname= el_actualeducationorganization.xpath( './KindName' ).pop().text
		acteducorg.typename= el_actualeducationorganization.xpath( './TypeName' ).pop().text
		acteducorg.regionname= el_actualeducationorganization.xpath( './RegionName' ).pop().text
		acteducorg.federaldistrictshortname= el_actualeducationorganization.xpath( './FederalDistrictShortName' ).pop().text
		acteducorg.federaldistrictname= el_actualeducationorganization.xpath( './FederalDistrictName' ).pop().text

	def supplement( self, el_supplement, certificate ):
		supp = Supplement()
		certificate.supplements.append( supp )

		supp.id_from_xml = el_supplement.xpath( './Id' ).pop().text
		supp.statusname= el_supplement.xpath( './StatusName' ).pop().text
		supp.statuscode= el_supplement.xpath( './StatusCode' ).pop().text
		supp.number= el_supplement.xpath( './Number' ).pop().text
		supp.serialnumber= el_supplement.xpath( './SerialNumber' ).pop().text
		supp.formnumber= el_supplement.xpath( './FormNumber' ).pop().text
		supp.issuedate= el_supplement.xpath( './IssueDate' ).pop().text
		supp.isforbranch= el_supplement.xpath( './IsForBranch' ).pop().text
		supp.note= el_supplement.xpath( './Note' ).pop().text
		supp.eduorgfullname= el_supplement.xpath( './EduOrgFullName' ).pop().text
		supp.eduorgshortname= el_supplement.xpath( './EduOrgShortName' ).pop().text
		supp.eduorgaddress= el_supplement.xpath( './EduOrgAddress' ).pop().text
		supp.eduorgkpp= el_supplement.xpath( './EduOrgKPP' ).pop().text

		# ActualEducationOrganization
		el_actualeducationorganization = el_supplement.xpath( './ActualEducationOrganization' ).pop()
		self.actualeducationorganization( el_actualeducationorganization, supp )

		# EducationalProgram
		el_educationalprograms = el_supplement.xpath( './EducationalPrograms' ).pop()
		for el_educationalprogram in el_educationalprograms.iterchildren( tag='EducationalProgram' ):
			self.educationalprogram( el_educationalprogram, supp )

	def educationalprogram( self, el_educationalprogram, supp ):
		educprog = EducationalProgram()
		supp.educationalprograms.append( educprog )

		educprog.id_from_xml = el_educationalprogram.xpath( './Id' ).pop().text
		educprog.typename= el_educationalprogram.xpath( './TypeName' ).pop().text
		educprog.edulevelname= el_educationalprogram.xpath( './EduLevelName' ).pop().text
		educprog.programmname= el_educationalprogram.xpath( './ProgrammName' ).pop().text
		educprog.programmcode= el_educationalprogram.xpath( './ProgrammCode' ).pop().text
		educprog.ugsname= el_educationalprogram.xpath( './UGSName' ).pop().text
		educprog.ugscode= el_educationalprogram.xpath( './UGSCode' ).pop().text
		educprog.edunormativeperiod= el_educationalprogram.xpath( './EduNormativePeriod' ).pop().text
		educprog.qualification= el_educationalprogram.xpath( './Qualification' ).pop().text
		educprog.isaccredited= el_educationalprogram.xpath( './IsAccredited' ).pop().text
		educprog.iscanceled= el_educationalprogram.xpath( './IsCanceled' ).pop().text
		educprog.issuspended= el_educationalprogram.xpath( './IsSuspended' ).pop().text

	def decision( self, el_decision, certificate ):
		dec = Decision()
		certificate.decisions.append( dec )

		dec.id_from_xml = el_decision.xpath( './Id' ).pop().text
		dec.decisiontypename= el_decision.xpath( './DecisionTypeName' ).pop().text
		dec.orderdocumentnumber= el_decision.xpath( './OrderDocumentNumber' ).pop().text
		dec.orderdocumentkind= el_decision.xpath( './OrderDocumentKind' ).pop().text
		dec.decisiondate= el_decision.xpath( './DecisionDate' ).pop().text