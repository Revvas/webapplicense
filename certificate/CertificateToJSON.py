
from json import dumps

from app import db

from .models import Certificate, ActualEducationOrganization, Supplement


def CertificateToJson( _id ):  # 2/5
	session = db.session
	main_dict = {}
	for instance in session.query( Certificate ).filter( Certificate.id == _id ):
		actual_education_organization_ar = []
		supplement_ar = []  #
		decisions_ar = []  #

		main_dict['id'] = instance.id
		main_dict['isfederal'] = instance.isfederal
		main_dict['id_from_xml'] = instance.id_from_xml
		main_dict['statusname'] = instance.statusname
		main_dict['typename'] = instance.typename
		main_dict['regionname'] = instance.regionname
		main_dict['regioncode'] = instance.regioncode
		main_dict['federaldistrictname'] = instance.federaldistrictname
		main_dict['federaldistrictshortname'] = instance.federaldistrictshortname
		main_dict['regnumber'] = instance.regnumber
		main_dict['serialnumber'] = instance.serialnumber
		main_dict['formnumber'] = instance.formnumber
		main_dict['issuedate'] = instance.issuedate
		main_dict['enddate'] = instance.enddate
		main_dict['controlorgan'] = instance.controlorgan
		main_dict['postaddress'] = instance.postaddress
		main_dict['eduorgfullname'] = instance.eduorgfullname
		main_dict['eduorgshortname'] = instance.eduorgshortname
		main_dict['eduorginn'] = instance.eduorginn
		main_dict['eduorgkpp'] = instance.eduorgkpp
		main_dict['eduorgogrn'] = instance.eduorgogrn
		main_dict['individualentrepreneurlastname'] = instance.individualentrepreneurlastname
		main_dict['individualentrepreneurfirstname'] = instance.individualentrepreneurfirstname
		main_dict['individualentrepreneurmiddlename'] = instance.individualentrepreneurmiddlename
		main_dict['individualentrepreneuraddress'] = instance.individualentrepreneuraddress
		main_dict['individualentrepreneuregrip'] = instance.individualentrepreneuregrip
		main_dict['individualentrepreneurinn'] = instance.individualentrepreneurinn
		# !!!!!!!!!!!!!!
		main_dict['actual_education_organization_id'] = instance.actual_education_organization_id
		# !!!!!!!!!!!!!!!!!!!

		for instance in session.query( ActualEducationOrganization ).filter( ActualEducationOrganization.id == instance.actual_education_organization_id ):
			actual_education_organization_dict = {}

			actual_education_organization_dict['id'] = instance.id
			actual_education_organization_dict['id_from_xml'] = instance.id_from_xml
			actual_education_organization_dict['fullname'] = fullname
			actual_education_organization_dict['shortname'] = shortname
			actual_education_organization_dict['headeduorgid'] = headeduorgid
			actual_education_organization_dict['isbranch'] = isbranch
			actual_education_organization_dict['postaddress'] = postaddress
			actual_education_organization_dict['phone'] = phone
			actual_education_organization_dict['fax'] = fax
			actual_education_organization_dict['email'] = email
			actual_education_organization_dict['website'] = website
			actual_education_organization_dict['ogrn'] = ogrn
			actual_education_organization_dict['inn'] = inn
			actual_education_organization_dict['kpp'] = kpp
			actual_education_organization_dict['headpost'] = headpost
			actual_education_organization_dict['headname'] = headname
			actual_education_organization_dict['formname'] = formname
			actual_education_organization_dict['kindname'] = kindname
			actual_education_organization_dict['typename'] = typename
			actual_education_organization_dict['regionname'] = regionname
			actual_education_organization_dict['federaldistrictshortname'] = federaldistrictshortname
			actual_education_organization_dict['federaldistrictname'] = federaldistrictname

			actual_education_organization_ar.append( actual_education_organization_dict )
		main_dict['actual_education_organization'] = actual_education_organization_ar
		#
		for instance in session.query( Supplement ).filter( Supplement.license_id == _id ):
			supplement_dict = {}
			actual_education_organization_ar = []
			educationalprograms = []

			supplement_dict['id'] = instance.id

			id_from_xml = Column( String )
			statusname = Column( String )
			statuscode = Column( String )
			number = Column( String )
			serialnumber = Column( String )
			formnumber = Column( String )
			issuedate = Column( String )
			isforbranch = Column( String )
			note = Column( String )
			eduorgfullname = Column( String )
			eduorgshortname = Column( String )
			eduorgaddress = Column( String )
			eduorgkpp = Column( String )
			# !!!!!!!!!!!!!!
			certificate_id = Column( Integer, ForeignKey( 'certificates.id' ) )
			actual_education_organization_id = Column( Integer, ForeignKey( 'actual_education_organizations.id' ) )
			# !!!!!!!!!!

			actual_education_organization = relationship
			educationalprograms = relationship

		main_dict['supplements'] = supplement_ar
		#
		decisions = relationship( 'Decision', uselist=True )

	json_string = dumps( main_dict )
	return json_string
