
from app import ma
from .models import Certificate
from .models import ActualEducationOrganization
from .models import Supplement
from .models import EducationalProgram
from .models import Decision


class SupplementSchema( ma.Schema ):
	class Meta:
		model = Supplement
		fields = (
			'id',
			'id_from_xml',
			'statusname',
			)


class CertificateSchema( ma.Schema ):
	class Meta:
		model = Certificate
		fields = (
			'id',
			'isfederal',
			'id_from_xml',
			'supplements',
			)

	supplements = ma.Nested( SupplementSchema, many=True )


certificate_schema = CertificateSchema()
