
from sqlalchemy import Column, Integer, String, ForeignKey, MetaData
from sqlalchemy.orm import relationship

from app import db


SCHEMA_NAME = 'certificates'


class Certificate( db.Model ):
	__tablename__ = 'certificates'
	__table_args__ = { "schema": SCHEMA_NAME }

	id = Column( Integer, primary_key=True )

	isfederal = Column( String )
	id_from_xml = Column( String )
	statusname = Column( String )
	typename = Column( String )
	regionname = Column( String )
	regioncode = Column( String )
	federaldistrictname = Column( String )
	federaldistrictshortname = Column( String )
	regnumber = Column( String )
	serialnumber = Column( String )
	formnumber = Column( String )
	issuedate = Column( String )
	enddate = Column( String )
	controlorgan = Column( String )
	postaddress = Column( String )
	eduorgfullname = Column( String )
	eduorgshortname = Column( String )
	eduorginn = Column( String )
	eduorgkpp = Column( String )
	eduorgogrn = Column( String )
	individualentrepreneurlastname = Column( String )
	individualentrepreneurfirstname = Column( String )
	individualentrepreneurmiddlename = Column( String )
	individualentrepreneuraddress = Column( String )
	individualentrepreneuregrip = Column( String )
	individualentrepreneurinn = Column( String )

	actual_education_organization_id = Column( Integer, ForeignKey( f'{SCHEMA_NAME}.actual_education_organizations.id' ) )
	actual_education_organization = relationship( 'ActualEducationOrganization', uselist=False )

	supplements = relationship( 'Supplement', uselist=True )
	decisions = relationship( 'Decision', uselist=True )


class ActualEducationOrganization( db.Model ):
	__tablename__ = 'actual_education_organizations'
	__table_args__ = { "schema": SCHEMA_NAME }

	id = Column( Integer, primary_key=True )

	id_from_xml = Column( String )
	fullname = Column( String )
	shortname = Column( String )
	headeduorgid = Column( String )
	isbranch = Column( String )
	postaddress = Column( String )
	phone = Column( String )
	fax = Column( String )
	email = Column( String )
	website = Column( String )
	ogrn = Column( String )
	inn = Column( String )
	kpp = Column( String )
	headpost = Column( String )
	headname = Column( String )
	formname = Column( String )
	kindname = Column( String )
	typename = Column( String )
	regionname = Column( String )
	federaldistrictshortname = Column( String )
	federaldistrictname = Column( String )


class Supplement( db.Model ):
	__tablename__ = 'supplements'
	__table_args__ = { "schema": SCHEMA_NAME }

	id = Column( Integer, primary_key=True )

	id_from_xml = Column( String )
	statusname = Column( String )
	statuscode = Column( String )
	number = Column( String )
	serialnumber = Column( String )
	formnumber = Column( String )
	issuedate = Column( String )
	isforbranch = Column( String )
	note = Column( String )
	eduorgfullname = Column( String )
	eduorgshortname = Column( String )
	eduorgaddress = Column( String )
	eduorgkpp = Column( String )

	certificate_id = Column( Integer, ForeignKey( f'{SCHEMA_NAME}.certificates.id' ) )

	actual_education_organization_id = Column( Integer, ForeignKey( ActualEducationOrganization.id ) )
	actual_education_organization = relationship( ActualEducationOrganization, uselist=False )

	educationalprograms = relationship( 'EducationalProgram', uselist=True )


class EducationalProgram( db.Model ):
	__tablename__ = 'educational_programs'
	__table_args__ = { "schema": SCHEMA_NAME }

	id = Column(Integer, primary_key=True)

	id_from_xml = Column( String )
	typename = Column( String )
	edulevelname = Column( String )
	programmname = Column( String )
	programmcode = Column( String )
	ugsname = Column( String )
	ugscode = Column( String )
	edunormativeperiod = Column( String )
	qualification = Column( String )
	isaccredited = Column( String )
	iscanceled = Column( String )
	issuspended = Column( String )

	supplement_id = Column( Integer, ForeignKey( Supplement.id ) )


class Decision( db.Model ):
	__tablename__ = 'decisions'
	__table_args__ = { "schema": SCHEMA_NAME }

	id = Column( Integer, primary_key=True )

	id_from_xml = Column( String )
	decisiontypename = Column( String )
	orderdocumentnumber = Column( String )
	orderdocumentkind = Column( String )
	decisiondate = Column( String )

	certificate_id = Column( Integer, ForeignKey( Certificate.id ) )
