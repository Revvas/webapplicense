
from json import dumps

from app import db

from .models import License, L_Supplement, LicensedProgram


def LicenseToJson( _id ):
	session = db.session
	main_dict = {}
	for instance in session.query( License ).filter( License.id == _id ):
		supplement_ar = []
		main_dict['id'] = instance.id
		main_dict['sys_guid'] = instance.sys_guid
		main_dict['school_guid'] = instance.school_guid
		main_dict['status_name'] = instance.status_name
		main_dict['school_name'] = instance.school_name
		main_dict['short_name'] = instance.short_name
		main_dict['school_type_name'] = instance.school_type_name
		main_dict['law_address'] = instance.law_address
		main_dict['org_name'] = instance.org_name
		main_dict['reg_num'] = instance.reg_num
		main_dict['date_lic_doc'] = instance.date_lic_doc
		main_dict['date_end'] = instance.date_end

		for instance in session.query( L_Supplement ).filter( L_Supplement.license_id == _id ):
			supplement_dict = {}
			licensed_program_ar = []
			supplement_dict['id'] = instance.id
			supplement_dict['license_fk'] = instance.license_fk
			supplement_dict['number'] = instance.number
			supplement_dict['status_name'] = instance.status_name
			supplement_dict['school_guid'] = instance.school_guid
			supplement_dict['school_name'] = instance.school_name
			supplement_dict['short_name'] = instance.short_name
			supplement_dict['law_address'] = instance.law_address
			supplement_dict['org_name'] = instance.org_name
			supplement_dict['num_lic_doc'] = instance.num_lic_doc
			supplement_dict['date_lic_doc'] = instance.date_lic_doc
			supplement_dict['sys_guid'] = instance.sys_guid
			supplement_dict['license_id'] = instance.license_id

			for instance in session.query( LicensedProgram ).filter( LicensedProgram.supplement_id == instance.id ):
				licensed_program_dict = {}
				licensed_program_dict['id'] = instance.id
				licensed_program_dict['supplement_fk'] = instance.supplement_fk
				licensed_program_dict['edu_program_type'] = instance.edu_program_type
				licensed_program_dict['code'] = instance.code
				licensed_program_dict['name'] = instance.name
				licensed_program_dict['edu_level_name'] = instance.edu_level_name
				licensed_program_dict['edu_program_kind'] = instance.edu_program_kind
				licensed_program_dict['qualification_code'] = instance.qualification_code
				licensed_program_dict['qualification_name'] = instance.qualification_name
				licensed_program_dict['sys_guid'] = instance.sys_guid
				licensed_program_dict['supplement_id'] = instance.supplement_id

				licensed_program_ar.append( licensed_program_dict )

			supplement_dict['licensed_programs'] = licensed_program_ar

			supplement_ar.append( supplement_dict )

		main_dict['supplements'] = supplement_ar

	json_string = dumps( main_dict )
	return json_string
