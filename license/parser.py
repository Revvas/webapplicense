
from lxml import etree

from app import db
from .models import License, L_Supplement, LicensedProgram


session = db.session


def fast_iter( context, func ):
	for event, elem in context:
		func( elem )
		session.commit()
		elem.clear()
		print( "session complete" )
		while elem.getprevious() is not None:
			del elem.getparent()[0]
	del context


class Parser( object ):
	def __init__( self ):
		pass

	def run( self, infile ):
		context = etree.iterparse( infile.absolute().as_posix(), events=('end',), tag='license' )
		fast_iter( context, self.license )

	def license( self, elem ):
		license = License()
		session.add( license )

		# #Licenses
		license.sys_guid = elem.xpath( './sysGuid' ).pop().text
		license.school_guid = elem.xpath( './schoolGuid' ).pop().text
		license.status_name = elem.xpath( './statusName' ).pop().text
		license.school_name = elem.xpath( './schoolName' ).pop().text
		license.short_name = elem.xpath( './shortName' ).pop().text
		license.school_type_name = elem.xpath( './schoolTypeName' ).pop().text
		license.law_address = elem.xpath( './lawAddress' ).pop().text
		license.org_name = elem.xpath( './orgName' ).pop().text
		license.reg_num = elem.xpath( './regNum' ).pop().text
		license.date_lic_doc = elem.xpath( './dateLicDoc' ).pop().text
		license.date_end = elem.xpath( './dateEnd' ).pop().text

		# Supplements
		el_supplements = elem.xpath( './supplements' ).pop()
		for el_supplement in el_supplements.iterchildren( tag='supplement' ):
			self.supplement( el_supplement, license )

	def supplement( self, el_supplement, license ):
		supp = L_Supplement()
		license.supplements.append( supp )

		supp.license_fk = el_supplement.xpath( './licenseFK' ).pop().text
		supp.number = el_supplement.xpath( './number' ).pop().text
		supp.status_name = el_supplement.xpath( './statusName' ).pop().text
		supp.school_guid = el_supplement.xpath( './schoolGuid' ).pop().text
		supp.school_name = el_supplement.xpath( './schoolName' ).pop().text
		supp.short_name = el_supplement.xpath( './shortName' ).pop().text
		supp.law_address = el_supplement.xpath( './lawAddress' ).pop().text
		supp.org_name = el_supplement.xpath( './orgName' ).pop().text
		supp.num_lic_doc = el_supplement.xpath( './numLicDoc' ).pop().text
		supp.date_lic_doc = el_supplement.xpath( './dateLicDoc' ).pop().text
		supp.sys_guid = el_supplement.xpath( './sysGuid' ).pop().text

		# Licensed Programs
		el_licensed_programs = el_supplement.xpath( './licensedPrograms' ).pop()
		for el_licensed_program in el_licensed_programs.iterchildren( tag='licensedProgram' ):
			self.licensed_program( el_licensed_program, supp )

	def licensed_program( self, el_licensed_program, supp ):
		licprog = LicensedProgram()
		supp.licensed_programs.append( licprog )

		licprog.supplement_fk = el_licensed_program.xpath( './supplementFk' ).pop().text
		licprog.edu_program_type = el_licensed_program.xpath( './eduProgramType' ).pop().text
		licprog.code = el_licensed_program.xpath( './code' ).pop().text
		licprog.name = el_licensed_program.xpath( './name' ).pop().text
		licprog.edu_level_name = el_licensed_program.xpath( './eduLevelName' ).pop().text
		licprog.edu_program_kind = el_licensed_program.xpath( './eduProgramKind' ).pop().text
		licprog.qualification_code = el_licensed_program.xpath( './qualificationCode' ).pop().text
		licprog.qualification_name = el_licensed_program.xpath( './qualificationName' ).pop().text
		licprog.sys_guid = el_licensed_program.xpath( './sysGuid' ).pop().text
