from sqlalchemy import Column, Integer, String, ForeignKey, MetaData
from sqlalchemy.orm import relationship

from app import db

SCHEMA_NAME = 'licenses'


class License( db.Model ):
	__tablename__ = 'licenses'
	__table_args__ = { "schema": SCHEMA_NAME }

	id = Column( Integer, primary_key=True )
	sys_guid = Column( String )
	school_guid = Column( String )
	status_name = Column( String )
	school_name = Column( String )
	short_name = Column( String )
	school_type_name = Column( String )
	law_address = Column( String )
	org_name = Column( String )
	reg_num = Column( String )
	date_lic_doc = Column( String )
	date_end = Column( String )

	supplements = relationship( "L_Supplement", uselist=True )


class L_Supplement( db.Model ):
	__tablename__ = 'supplements'
	__table_args__ = { "schema": SCHEMA_NAME }

	id = Column( Integer, primary_key=True )
	license_fk = Column( String )
	number = Column( String )
	status_name = Column( String )
	school_guid = Column( String )
	school_name = Column( String )
	short_name = Column( String )
	law_address = Column( String )
	org_name = Column( String )
	num_lic_doc = Column( String )
	date_lic_doc = Column( String )
	sys_guid = Column( String )

	license_id = Column( Integer, ForeignKey( License.id ) )
	licensed_programs = relationship( "LicensedProgram", uselist=True )


class LicensedProgram( db.Model ):
	__tablename__ = 'licensed_programs'
	__table_args__ = { "schema": SCHEMA_NAME }

	id = Column( Integer, primary_key=True )
	supplement_fk = Column( String )
	edu_program_type = Column( String )
	code = Column( String )
	name = Column( String )
	edu_level_name = Column( String )
	edu_program_kind = Column( String )
	qualification_code = Column( String )
	qualification_name = Column( String )
	sys_guid = Column( String )

	supplement_id = Column( Integer, ForeignKey( L_Supplement.id ) )
