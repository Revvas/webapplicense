
import os
from dotenv import load_dotenv


basedir = os.path.abspath( os.path.dirname( __file__ ) )
load_dotenv( os.path.join( basedir, '.env' ) )


class Config( object ):

	SQLALCHEMY_TRACK_MODIFICATIONS = False
	SQLALCHEMY_DATABASE_URI = '{dialect}+{driver}://{user}:{password}@{host}:{port}/{database}'.format(
			dialect='postgresql',
			driver='psycopg2',
			user='pguser',  # FIXME: change
			password='pass',  # FIXME: change
			host='127.0.0.1',
			port='5432',
			database='testwebmiigaik',  # FIXME: change
			)
