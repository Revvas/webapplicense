
from flask import jsonify
from flask import url_for
from flask import Blueprint

from app import ma
from license import LicenseToJson
from certificate import CertificateToJson
from certificate import Certificate
from certificate import certificate_schema


bp = Blueprint( 'app', __name__ )


@bp.route( '/' )
def slash():
	message = f"""
Use this work example for make a request:
	<a href="{url_for( '.req', sheme='license', id=2 )}">Тест</a>
	"""
	return message


@bp.route( '/request/<sheme>/<int:id>/' )
def req( sheme, id ):
	if sheme == "license":
		return LicenseToJson( id )

	elif sheme == "certificate":
		# cs = CertificateSchema
		obj = Certificate.query.get( id )
		print( obj )
		return certificate_schema.jsonify( obj )

	else:
		return f'Wrong request'
