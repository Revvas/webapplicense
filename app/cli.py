
import os
import click
import pathlib

from license import Parser as LicenseParser
from certificate import Parser as CertificateParser


def register( app ):
	@app.cli.group()
	def parse():
		"""Parser commands."""
		pass

	@parse.command( 'license', short_help="Parser License Data" )
	@click.argument( 'path', type=pathlib.Path )
	def license( path ):
		lp = LicenseParser()
		lp.run( path )

	@parse.command( 'certificate', short_help="Certificate License Data" )
	@click.argument( 'path', type=pathlib.Path )
	def license( path ):
		cp = CertificateParser()
		cp.run( path )
