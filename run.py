#!/usr/bin/env python3

import sys
from flask.cli import FlaskGroup
from app import create_app, db, cli


app = create_app()
cli.register( app )


if __name__ == '__main__':
	fg = FlaskGroup( create_app=lambda: app )
	fg.main( sys.argv[1:] )
